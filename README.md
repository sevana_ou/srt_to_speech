The idea was to use Google's latest WaveNet technology to convert subtitles to audio.
We use it for experiments.
Script accepts name of .srt file (or any extension supported by pysub-parser) and produces resulting audio to output_speech.wav

# Requirements
- Python 3 
- pysub-parser (pip3 install pysub-parser)
- google-cloud-texttospeech (pip3 install google-cloud-texttospeech)

# Install
- Please don't forget to install pysub-parser & google-cloud-texttospeech
- Please generate your credentials file and put it near the script under tts_credentials.json name.
- Use https://cloud.google.com/text-to-speech/docs/quickstart-client-libraries as manual.




