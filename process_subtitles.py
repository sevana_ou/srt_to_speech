#!/usr/local/bin/python3

import os
import sys
import wave
import typing
import datetime

from pysubparser import parser
from pysubparser.cleaners import ascii, brackets, formatting, lower_case
from google.cloud import texttospeech

# Name of Google's credentials file
TTS_CREDENTIALS = "tts_credentials.json"
OUTPUT_FILENAME = "output_speech.wav"

# To support coloring in output
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# Converts datetime.time to milliseconds
def time_to_milliseconds(t: datetime.time) -> float:
    return (t.hour * 3600 + t.minute * 60 + t.second) * 1000 + t.microsecond / 1000


# Emits silence to audio output_file
def append_silence(output_file: wave.Wave_write, milliseconds: float):
    # Get size in bytes
    length = int(milliseconds * output_file.getsampwidth() * output_file.getframerate() / 1000 * output_file.getnchannels())
    
    # Write zero bytes to output file
    output_file.writeframes(bytes(bytearray(length)))


# Check if input file name is set
if len(sys.argv) < 2:
    print("Usage: process_subtitles.py <path_to_subtitles_file>")
    sys.exit(1)

if not os.path.exists(TTS_CREDENTIALS):
    print("Sorry, TTS credentials file is not found. Exiting.")
    sys.exit(1)

# Load & parse subtitles file
subtitles = parser.parse(sys.argv[1])

# Set pointer to credentials file
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = TTS_CREDENTIALS

# Instantiates a TTS client
client = texttospeech.TextToSpeechClient()

# Set the text input to be synthesized
# synthesis_input = texttospeech.types.SynthesisInput(text="Hello, World!")

# Build the voice request, select the language code ("en-US") and the ssml
# voice gender ("neutral")
voice = texttospeech.types.VoiceSelectionParams(
    language_code='en-US',
    ssml_gender=texttospeech.enums.SsmlVoiceGender.NEUTRAL)

# Use .wav files
audio_config = texttospeech.types.AudioConfig(
    audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16)

# Clear text in brackets
subtitles = brackets.clean(subtitles)
subtitle_idx = 0

# "End speech" timestamp in milliseconds - point where speech ends
last_time: float = None

# File to write
output_file: wave.Wave_write = None

for subtitle in subtitles:
    # Log current chunk times & text
    print(f'{subtitle.start} > {subtitle.end}')
    print(subtitle.text)

    # Look how much silence is needed
    if last_time is None:
        silence_length = time_to_milliseconds(subtitle.start)
    else:
        silence_length = time_to_milliseconds(subtitle.start) - last_time
    
    # Next offset
    # start_time = time_to_milliseconds(subtitle.end)

    # Build .wav file for text
    synthesis_input = texttospeech.types.SynthesisInput(text=subtitle.text)
    response = client.synthesize_speech(synthesis_input, voice, audio_config)
    
    # The response's audio_content is binary with WAV file header inside.
    # Write it to disk and open again to skip file header later and minimize this script
    filename = f"subtitle_chunk.wav"
    with open(filename, "wb") as out:
         # Write the response to the output file.
        out.write(response.audio_content)
        out.close()

    with wave.open(filename, 'rb') as chunk_file:
        # Read all frames
        frames = chunk_file.readframes(chunk_file.getnframes())

        # Open output file if needed
        if output_file is None:
            output_file = wave.open(OUTPUT_FILENAME, 'wb')
            output_file.setframerate(chunk_file.getframerate())
            output_file.setnchannels(chunk_file.getnchannels())
            output_file.setsampwidth(chunk_file.getsampwidth())
        
        # Add silence (or another background audio)
        if silence_length > 0:
            append_silence(output_file, silence_length)
        else:
            print(f"{bcolors.WARNING}Negative offset between chunks: {silence_length / 1000}s{bcolors.ENDC}")

        # Add synthesized audio
        output_file.writeframes(frames)

        # Find next time offset
        last_time = time_to_milliseconds(subtitle.start) + int((chunk_file.getnframes() / chunk_file.getframerate() * 1000))

if output_file is not None:
    output_file.close()

# Clear google's credentials
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = ''

print(f"{bcolors.OKGREEN}Done{bcolors.ENDC}")